

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Clearup</title>

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css">

        <!-- Custom Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

        <!-- Plugin CSS -->
        <link rel="stylesheet" href="css/animate.min.css" type="text/css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/creative.css" type="text/css">
        <link rel="stylesheet" href="css/slider.css" type="text/css">
        <link rel="stylesheet" href="css/clearup.css" type="text/css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.css" type="text/css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
        <script src="js/clearup.js"></script>
        <script src="js/jquery-1.10.2.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/slider.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script>
            $(function () {
                $('#sendmessagebtn1').on('click', function () {
                    $('#newmessage1').show();
                    $('#sendmessageinput1').find('input').val('');
                    $('#sendmessageinput1').find('textarea').val('');
                });
            });
        </script>        
    </head>
    <body>
        <div class="container">
            <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
                <!--<div class="container-fluid"> -->
                <div style="background-color: #00B16A">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="row">
                        <div class="col-md-2 navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand page-scroll" href="homePage.html">Clearup</a>
                        </div>


                        <div class="col-md-5 mid" role="navigation">

                            <!--<a class="navbar-brand" href="http://localhost:8080/test/" title="My Project" rel="home">My Project</a> -->

                            <form class="navbar-form pull-left" role="search" method="get" id="searchform" action="http://localhost:8084/Clearup/searchResults.html">
                                <div class="input-group">
                                    <div class="input-group-btn" >
                                        <button type="button" id="searchcategory" value="ALL" class="btn btn-default dropdown-toggle" data-toggle="dropdown" >ALL<span class="caret"></span></button>

                                        <ul id="searchdropdown" class="dropdown-menu" >
                                            <li><a href="javascript:void(0)">BUY</a></li>
                                            <li><a href="javascript:void(0)">RENT</a></li>
                                            <li><a href="javascript:void(0)">GIVEAWAY</a></li>
                                            <li><a href="javascript:void(0)">SWAP</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)">ALL</a></li>          
                                        </ul>
                                    </div>
                                    <input type="text" class="form-control" value="" placeholder="Search..." name="s" id="s" style="border-color: grey;">
                                    <div class="input-group-btn">
                                        <button type="submit" id="searchsubmit" value="Search" class="btn" style="background-color: white;border-color: grey;color:#00B16A">
                                            <span class="glyphicon glyphicon-search"></span></button>
                                    </div>
                                </div>
                            </form> 

                            <!-- /.navbar-header -->


                            <!--<div class="collapse navbar-collapse upper-navbar">    	 
                                    <ul id="menu-topmenu" class="nav navbar-nav navbar-right">
                                            <li id="menu-item-1"><a href="#one">Page I</a></li>
                                                    <li id="menu-item-2"><a href="#two">Page II</a></li>
                                    <li id="menu-item-3"><a href="#three">Page III</a></li>
                                            </ul>	  
                            </div><!-- /.navbar-collapse -->
                            <!--</div> -->
                        </div>



                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="col-md-5 collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class = "row">
                                <ul class="nav navbar-nav navbar-right">

                                    <li>

                                        <a href="postlistings.html" style="color: white;padding-top:15px; "> 
                                            <strong> Post Listings</strong>
                                        </a>

                                    </li>

                                    <li>
                                        <a href="myListing.html">
                                            <!--<i class="fa fa-2x fa-bell wow bounceIn text-primary"></i> -->
                                            <img src="img/interface.png" alt="messages" style="height: 30px;">
                                        </a>
                                    </li>

                                    <li>
                                        <!--<a class="page-scroll" href="#portfolio"> -->
                                        <!-- <div class="user" style="background-image:url('http://placehold.it/350x150')"></div> -->
                                        <a href="#" >                                            <!--<i class="fa fa-4x fa-user wow bounceIn text-primary" data-wow-delay=".2s"></i> -->
                                            <img src="img/user_me.png" class="img-circle" alt="profile" style="height: 30px;">
                                        </a>

                                        <!-- /Users/vindhyakumarik/NetBeansProjects/TradeHub/web/img/IMG_5865.jpg -->

                                    </li>
                                    <li>
                                        <!-- <a class="page-scroll" href="#services">  -->
                                        <a href="#">
                                            <i class="fa fa-2x fa-caret-down wow bounceIn text-primary" ></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="page-scroll"></a>
                                    </li>

                                    <!-- <li>
                                        <a class="page-scroll" href="#contact">Contact</a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
            </nav>
        </div>
        <br><br>
        <div class="container">

            <div class="row">
                <div class="col-md-4" style="border-right: 1px solid #ccc;">
                    <div class="row profile">
                        <div class="col-md-12">
                            <div class="profile-sidebar">
                                <ul class="nav nav-stacked">
                                    <li class="active">
                                        <a data-toggle="pill" href="#message1">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="profile-userpic">
                                                        <img src="img/people1.png" class="img-responsive" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <h5>Vindhya</h5>
                                                    <p style="font-size: 12px;">Regarding the listing for the ultimate cycle...</p>
                                                </div>                                                         
                                            </div>                                      
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="pill" href="#message2">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="profile-userpic">
                                                        <img src="img/people.png" class="img-responsive" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <h5>Sandhya</h5>
                                                    <p style="font-size: 12px;">Regarding the listing for the ultimate cycle...</p>
                                                </div>                                                         
                                            </div>                                      
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="pill" href="#message3">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="profile-userpic">
                                                        <img src="img/people2.png" class="img-responsive" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <h5>Vashist</h5>
                                                    <p style="font-size: 12px;">Regarding the listing for the ultimate cycle...</p>
                                                </div>                                                         
                                            </div>                                      
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="pill" href="#message4">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="profile-userpic">
                                                        <img src="img/people3.png" class="img-responsive" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <h5>John</h5>
                                                    <p style="font-size: 12px;">Regarding the listing for the ultimate cycle...</p>
                                                </div>                                                         
                                            </div>                                      
                                        </a>
                                    </li>                                            
                                </ul>			
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-md-8">
                    <div class="tab-content menu-content">
                        <div id="message1" class="tab-pane fade in active">

                            <!-- Comment -->
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="img/peoplet.png" alt="">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">Regarding the ultimate cycle listing for rent
                                    <small>April 25, 2016 at 5:30 PM</small>
                                    </h4>
                                    I would like to rent it for 2 weeks from next week. please let me know the availability.
                                </div>
                            </div>

                            <!-- Comment -->
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="img/peoplet1.png" alt="">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">The ultimate cycle for rent.
                                    <small>April 25, 2016 at 5:30 PM</small>
                                    </h4>
                                    Yes sure. You can pick the cycle and rent is as mentioned in the listing.
                                    <!-- End Nested Comment -->
                                </div>
                            </div>
                            <!-- Nested Comment -->
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="img/peoplet.png" alt="">
                                </a>                                 
                                <div class="media-bodyp">
                                    <h4 class="media-heading">Information regarding the confirmation
                                        <small>April 25, 2016 at 5:30 PM</small>
                                    </h4>
                                    When should I confirm about this.                                   
                                </div>                                        
                            </div>                             
                            <hr/> 
                            <div id="newmessage1" class="media" style="display:none;">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="img/peoplet1.png" alt="">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">Need Confirmation
                                        <small>April 25, 2016 at 7:30 PM</small>
                                    </h4>
                                    Please let me know the confirmation by tomorrow.
                                </div>
                            </div>                            
                            
                            <!-- Comments Form -->
                            <div class="well" id="sendmessageinput1">
                                <h4>Send a Message: </h4>
                                <form role="form">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="messageTitle1" placeholder="Enter Title">
                                    </div>                                    
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3"></textarea>
                                    </div>
                                    <button type="button" id="sendmessagebtn1" class="btn btn-primary">Submit</button>
                                </form>
                            </div>

                            <hr>
                        <!-- message 1 ends-->    
                        </div>
                        <div id="message2" class="tab-pane fade">
                            <h3>Menu 2</h3>
                            <p></p>
                        <!-- message 2 ends-->
                        </div>
                        <div id="message3" class="tab-pane fade">
                            <h3>Menu 3</h3>
                            <p></p>
                        <!-- message 3 ends-->
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="row"></div>
            <div class="row"></div>            
        </div>
    </body>
</html>
