<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>ClearUp</title>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

        <!-- Custom Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

        <!-- Plugin CSS -->
        <link rel="stylesheet" href="css/animate.min.css" type="text/css">
        <link rel="stylesheet" href="css/clearup.css" type="text/css">
        <link rel="stylesheet" href="css/creative.css" type="text/css">

        <script src="js/clearup.js" ></script>
        <link rel="stylesheet" href="css/mystyle.css" type="text/css">
        <script>
            $(function () {
                $('#boston').on('click', function () {
                    $('#bostonrow').hide();

                });

                $('#sendmessagebtn').on('click', function () {
                    $('#beforesend').hide();
                    $('#aftersend').show();
                });

                $('#sendmessagbtn').on('click', function () {
                    $('#beforesend').show();
                    $('#aftersend').hide();
                });


            });
        </script>
    </head>
    <body id="page-top">

        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
            <!--<div class="container-fluid"> -->
            <div style="background-color: #00B16A">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="row">
                <div class="col-lg-3 navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="homePage.html">Clearup</a>
                </div>


                <div class=" mid" role="navigation">
                    
                        <!--<a class="navbar-brand" href="http://localhost:8080/test/" title="My Project" rel="home">My Project</a> -->

                        <form class="navbar-form pull-left" role="search" method="get" id="searchform" action="http://localhost:8084/Clearup/searchResults.html">
                            <div class="input-group">
                                <div class="input-group-btn" >
                                    <button type="button" id="searchcategory" value="ALL" class="btn btn-primary dropdown-toggle" style="border-radius: 0;background-color: whitesmoke" data-toggle="dropdown" >ALL<span class="caret"></span></button>

                                    <ul id="searchdropdown" class="dropdown-menu" >
                                        <li><a href="javascript:void(0)">BUY</a></li>
                                        <li><a href="javascript:void(0)">RENT</a></li>
                                        <li><a href="javascript:void(0)">GIVEAWAY</a></li>
                                        <li><a href="javascript:void(0)">SWAP</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:void(0)">ALL</a></li>          
                                    </ul>
                                </div>
                                <input type="text" class="form-control" value="" placeholder="Search..." name="s" id="s" style="border-color: #00B16A;">
                                <div class="input-group-btn">
                                    <button type="submit" id="searchsubmit" value="Search" class="btn" style="border-radius: 0;background-color: white;border-color: #00B16A;color:#00B16A">
                                        <span class="glyphicon glyphicon-search"></span></button>
                                </div>
                            </div>
                        </form> 

                    <!-- /.navbar-header -->


                    <!--<div class="collapse navbar-collapse upper-navbar">    	 
                            <ul id="menu-topmenu" class="nav navbar-nav navbar-right">
                                    <li id="menu-item-1"><a href="#one">Page I</a></li>
                                            <li id="menu-item-2"><a href="#two">Page II</a></li>
                            <li id="menu-item-3"><a href="#three">Page III</a></li>
                                    </ul>	  
                    </div><!-- /.navbar-collapse -->
                    <!--</div> -->
                </div>



                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class=" collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class = "row">
                        <ul class="nav navbar-nav navbar-right">

                            <li>

                                <a href="postlistings.html" style="color: white;padding-top:15px; "> 
                                    <strong> Post Listings</strong>
                                </a>

                            </li>

                            <li>
                                <a href="myListing.html">
                                    <!--<i class="fa fa-2x fa-bell wow bounceIn text-primary"></i> -->
                                    <img src="img/interface.png" alt="messages" style="height: 30px;">
                                </a>
                            </li>

                            <li>
                                <!--<a class="page-scroll" href="#portfolio"> -->
                                <!-- <div class="user" style="background-image:url('http://placehold.it/350x150')"></div> -->
                                <a href="#" >                                            <!--<i class="fa fa-4x fa-user wow bounceIn text-primary" data-wow-delay=".2s"></i> -->
                                    <img src="img/user_me.png" class="img-circle" alt="profile" style="height: 30px;">
                                </a>

                                <!-- /Users/vindhyakumarik/NetBeansProjects/TradeHub/web/img/IMG_5865.jpg -->

                            </li>
                            <li>
                                <!-- <a class="page-scroll" href="#services">  -->
                                <a href="#">
                                    <i class="fa fa-2x fa-caret-down bounceIn text-primary" ></i>
                                </a>
                            </li>
                            <li>
                                <a class="page-scroll"></a>
                            </li>

                            <!-- <li>
                                <a class="page-scroll" href="#contact">Contact</a>
                            </li> -->
                        </ul>
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
            </div>
        </nav>
        <div class="container"> 
            <div class="row">

                <div class="col-md-4" style="background:#F1F3FA">
                    <!-- Profile sidebar -->
                    <div class="row profile">
                        <div class="col-md-12">
                            <div class="profile-sidebar">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic">
                                    <img src="img/profile_photo2.png" class="img-responsive" alt="">
                                </div>
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name">
                                        Vashist Suresh
                                    </div>
                                    <div class="profile-usertitle-job">
                                        Student
                                    </div>
                                </div>

                                <div class="profile-userbuttons">
                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#messagemodal" id="sendmessagbtn">Message</button>

                                </div>				
                            </div>
                        </div>
                    </div>

                    <div class="row profile">
                        <div class="col-md-12">
                            <div class="profile-sidebar">
                                <ul class="nav nav-pills nav-stacked">
                                    <li class="active"><a data-toggle="pill" href="#home"><i class="glyphicon glyphicon-home profilepadding"></i> Profile </a></li>
                                    <li><a data-toggle="pill" href="#menu2"><i class="glyphicon glyphicon-ok profilepadding"></i> Listings <span class="badge">3</span></a></li>
                                </ul>			
                            </div>
                        </div>
                    </div>			
                </div>
                <div class="col-md-8">
                    <div class="tab-content menu-content">

                        <div id="home" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel">
                                        <div class="panel-heading bg-primary">Personal Details</div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="firstname">FirstName</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control" id="firstname" placeholder="Vashist" readonly></input>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="lastname">LastName</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control" id="lastname" placeholder="Suresh" readonly></input>
                                                        </div>
                                                    </div>								  
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="occupation">Occupation</label>
                                                        <div class="col-sm-7"> 
                                                            <input type="text" class="form-control" id="occupation" placeholder="Student" readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="panel">
                                        <div class="panel-heading bg-primary">
                                            Contact Information 
                                        </div>
                                        <div class="panel-body">
                                            <div class="row" >                                               
                                                <div class="col-md-12">
                                                    <center><img src="img/test.PNG"></img></center>
                                                </div>
                                            </div>						
                                        </div>
                                    </div>
                                </div>

                            </div>			
                        </div>


                        <div id="messagemodal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" id="resetbutton" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Message Seller</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-box">
                                            <div id="beforesend">
                                                <div class="form-bottom">
                                                    <form role="form" action="" method="post" class="registration-form" id="sendmessageform">
                                                        <div class="form-group">
                                                            <label for="form-user-name"><i>Seller Name</i></label>
                                                            <input type="text" name="form-user-name" placeholder="Sandhya" class="form-user-name form-control" id="form-user-name" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="sr-only" for="form-message-title">Message Title</label>
                                                            <input type="text" name="form-message-title" placeholder="Title" class="form-user-name form-control" id="form-message-title" required>
                                                        </div>
                                                        <!--<div class="form-group" id="community_desc" style="display:none">
                                                                <div class="well">
                                                                <h3>Northeastern</h3>
                                                                <h5>Community for the northeastern university</h5>
                                                                </div>
                                                        </div>-->

                                                        <div class="form-group" id="message">
                                                            <label class="sr-only" for="comment">Message Details</label>
                                                            <textarea class="form-control" rows="5" id="comment" placeholder="Message Details" required></textarea>
                                                        </div>							
                                                        <button type="button" id="sendmessagebtn" class="btn btn-success btn-sm">Send Message</button>
                                                    </form>
                                                </div>
                                            </div>
                                            <div id="aftersend" style="display:none;">
                                                <div class="form-bottom">
                                                    Thank you for contacting the seller.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>	

                        <div id="messagemodal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" id="resetbutton" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Message Seller</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-box">
                                            <div id="beforesend">
                                                <div class="form-bottom">
                                                    <form role="form" action="" method="post" class="registration-form" id="sendmessageform">
                                                        <div class="form-group">
                                                            <label for="form-user-name"><i>Seller Name</i></label>
                                                            <input type="text" name="form-user-name" placeholder="Sandhya" class="form-user-name form-control" id="form-user-name" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="sr-only" for="form-message-title">Message Title</label>
                                                            <input type="text" name="form-message-title" placeholder="Title" class="form-user-name form-control" id="form-message-title" required>
                                                        </div>
                                                        <!--<div class="form-group" id="community_desc" style="display:none">
                                                                <div class="well">
                                                                <h3>Northeastern</h3>
                                                                <h5>Community for the northeastern university</h5>
                                                                </div>
                                                        </div>-->

                                                        <div class="form-group" id="message">
                                                            <label class="sr-only" for="comment">Message Details</label>
                                                            <textarea class="form-control" rows="5" id="comment" placeholder="Message Details" required></textarea>
                                                        </div>							
                                                        <button type="button" id="sendmessagebtn" class="btn btn-success btn-sm">Send Message</button>
                                                    </form>
                                                </div>
                                            </div>
                                            <div id="aftersend" style="display:none;">
                                                <div class="form-bottom">
                                                    Thank you for contacting the seller.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>	

                        <div id="menu1" class="tab-pane fade">
                            <h3>Menu 1</h3>
                            <p></p>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <h3>Listings of Vashist</h3>
                                    <section class="no-padding" id="portfolio">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-sm-6">
                        <a href="#" class="portfolio-box">
                            <div class="img-wrapper">
                                <img src="img/11-Sofa set.jpg" class="img-responsive img-grid" alt="">
                                <div class="description">
                                    <p class="description_content">200$</p>   
                                </div>
                                <div class="description-main">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <p class="description-main-content">Home Decor</p>
                                        </div>
                                        <div class="col-lg-8" style="text-align: right">
                                            <p class="description-main-content">Sofa set</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-6">
                        <a href="#" class="portfolio-box">
                            <div class="img-wrapper">
                                <img src="img/portfolio/2.jpg" class="img-responsive img-grid" alt="">
                                <i class="fa fa-2x fa-heart-o" >
                                </i>
                                <div class="description">
                                    <p class="description_content">20$</p>

                                </div>
                                <div class="description-main">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <p class="description-main-content">Book</p>
                                        </div>
                                        <div class="col-lg-8" style="text-align: right">
                                            <p class="description-main-content">Don't just stand there</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <a href="#" class="portfolio-box">
                            <div id="icon-i" class="img-wrapper" >
                                <img src="img/2.jpg" class="img-responsive img-grid" alt="" align="middle">
                                <i class="fa fa-2x fa-heart-o" >
                                </i>
                                <div class="description">
                                    <p class="description_content">110$</p>  
                                </div>
                                <div class="description-main">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <p class="description-main-content">Smart watch</p>
                                        </div>
                                        <div class="col-lg-8" >
                                            <p class="description-main-content" style="right: 0">A9 MTK2502A Smart Watch</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>


                    <div class="col-sm-6">
                        <a href="#" class="portfolio-box">
                            <div class="img-wrapper">
                                <img src="img/3.jpg" class="img-responsive img-grid" alt="">
                                <div class="description">
                                    <p class="description_content">300$</p>   
                                </div>
                                <div class="description-main">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <p class="description-main-content">Camera</p>
                                        </div>
                                        <div class="col-lg-8" style="text-align: right">
                                            <p class="description-main-content">Samsung camera 15px</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>


                    <div class="col-sm-6">
                        <a href="#" class="portfolio-box">
                            <div class="img-wrapper">
                                <img src="img/6-Tupperware.jpg" class="img-responsive img-grid" alt="">
                                <div class="description">
                                    <p class="description_content">40$</p>   
                                </div>
                                <div class="description-main">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <p class="description-main-content">Utilities</p>
                                        </div>
                                        <div class="col-lg-8" style="text-align: right">
                                            <p class="description-main-content">Tupperware vessels</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <a href="#" class="portfolio-box">
                            <div class="img-wrapper">
                                <img src="img/8-iphone5.jpg" class="img-responsive img-grid" alt="">
                                <div class="description">
                                    <p class="description_content">220$</p>   
                                </div>
                                <div class="description-main">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <p class="description-main-content">Mobile</p>
                                        </div>
                                        <div class="col-lg-8" style="text-align: right">
                                            <p class="description-main-content">iPhone5</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <a href="#" class="portfolio-box">
                            <div class="img-wrapper">
                                <img src="img/7-Tupperware-icsid.jpg" class="img-responsive img-grid" alt="8">
                                <div class="description">
                                    <p class="description_content">35$</p>   
                                </div>
                                <div class="description-main">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <p class="description-main-content">Utilities</p>
                                        </div>
                                        <div class="col-lg-8" style="text-align: right">
                                            <p class="description-main-content">Tupperware Boxes</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <a href="#" class="portfolio-box">
                            <div class="img-wrapper">
                                <img src="img/10-Bed.jpg" class="img-responsive img-grid" alt="">
                                <div class="description">
                                    <p class="description_content">300$</p>   
                                </div>
                                <div class="description-main">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <p class="description-main-content">Home Decor</p>
                                        </div>
                                        <div class="col-lg-8" style="text-align: right">
                                            <p class="description-main-content">Queen size bed</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-sm-6">
                        <a href="#" class="portfolio-box">
                            <div class="img-wrapper">
                                <img src="img/9-iPhone6s.jpg" class="img-responsive img-grid" alt="">
                                <div class="description">
                                    <p class="description_content">380$</p>   
                                </div>
                                <div class="description-main">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <p class="description-main-content">Mobile</p>
                                        </div>
                                        <div class="col-lg-8" style="text-align: right">
                                            <p class="description-main-content">iPhone 6s</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <h3>Communities</h3>	


                            <div class="row communityspacing">
                                <div class="col-md-6">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="img/neu-seal.jpg" alt="CLEMSON"></img>
                                            </div>
                                            <div class="col-md-8">
                                                Founded in 1889, Clemson University consists of six colleges: Agriculture, Forestry and Life Sciences; Architecture, Arts and Humanities; Business and Behavioral Sciences; Engineering and Science; Health and Human Development; and Education. As of 2015, Clemson University enrolled a total of 18,016 undergraduate students for the fall semester and 4,682 graduate students and the student/faculty ratio is 16:1.
                                            </div>								
                                        </div>
                                        <div class="row communityspacing">
                                            <div class="col-md-2">

                                            </div>	
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-4">
                                                <button class="btn btn-success" type="button">
                                                    Joined <span class="glyphicon glyphicon-ok"></span>
                                                </button>
                                            </div>								
                                        </div>							
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="img/bu-seal.jpg" alt="BU"></img>
                                            </div>
                                            <div class="col-md-8">
                                                Boston University (most commonly referred to as BU or otherwise known as Boston U.) is a private research university located in Boston with 3,800 faculty members and 33,000 students.
                                            </div>								
                                        </div>
                                        <div class="row communityspacing">
                                            <div class="col-md-2">

                                            </div>	
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-4">
                                                <button class="btn btn-success" type="button">
                                                    Joined <span class="glyphicon glyphicon-ok"></span>
                                                </button>
                                            </div>								
                                        </div>								
                                    </div>						
                                </div>				
                            </div>

                            <div class="row communityspacing">
                                <div class="col-md-6">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="img/mit-seal.jpg" alt="CLEMSON"></img>
                                            </div>
                                            <div class="col-md-8">
                                                Community for Massachussets Institute Of Technology. The university in located in Cambridge, Massachussets.
                                            </div>								
                                        </div>
                                        <div class="row communityspacing">
                                            <div class="col-md-2">

                                            </div>	
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-4">
                                                <button class="btn btn-success" type="button">
                                                    Join <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </div>								
                                        </div>							
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="img/neu-seal.jpg" alt="CLEMSON"></img>
                                            </div>
                                            <div class="col-md-8">
                                                Community for CLEMSON University. The university in located in CLEMSON, SC.
                                            </div>								
                                        </div>
                                        <div class="row communityspacing">
                                            <div class="col-md-2">

                                            </div>	
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-4">
                                                <button class="btn btn-success" type="button">
                                                    Join <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </div>								
                                        </div>							
                                    </div>						
                                </div>				
                            </div>				
                        </div>
                    </div>
                </div>
            </div>	

        </div>
    </body>
</html>