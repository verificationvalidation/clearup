<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>ClearUp</title>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

        <!-- Custom Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

        <!-- Plugin CSS -->
        <link rel="stylesheet" href="css/animate.min.css" type="text/css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/creative.css" type="text/css">
        <link rel="stylesheet" href="css/mystyle.css" type="text/css">
        <link rel="stylesheet" href="css/clearup.css" type="text/css">  

        <script src="js/clearup.js" ></script>
        <link rel="stylesheet" href="css/mystyle.css" type="text/css">        
        <script>
            $(function () {
                $('#boston').on('click', function () {
                    $('#bostonrow').hide();

                });
                $('#editcontactinfo').on('click', function () {
                    $('#personalemailid').attr('readonly', false);
                    $('#personalemailid').focus();
                    $('#personaladdress').attr('readonly', false);
                    
                    $('#personalphone').attr('readonly', false);
                    
                });
                $('#savebuttoncontact').on('click', function () {
                    $('#personalemailid').attr('readonly', true);
                    $('#personaladdress').attr('readonly', true);
                    
                    $('#personalphone').attr('readonly', true);                    
                    
                });               
                

            });
        </script>
    </head>
    <body>
        <div class="container">
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
            <!--<div class="container-fluid"> -->
            <div style="background-color: #00B16A">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="row">
                <div class="col-lg-3 navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="homePage.html">Clearup</a>
                </div>


                <div class=" mid" role="navigation">
                    
                        <!--<a class="navbar-brand" href="http://localhost:8080/test/" title="My Project" rel="home">My Project</a> -->

                        <form class="navbar-form pull-left" role="search" method="get" id="searchform" action="http://localhost:8084/Clearup/searchResults.html">
                            <div class="input-group">
                                <div class="input-group-btn" >
                                    <button type="button" id="searchcategory" value="ALL" class="btn btn-primary dropdown-toggle" style="border-radius: 0;background-color: whitesmoke" data-toggle="dropdown" >ALL<span class="caret"></span></button>

                                    <ul id="searchdropdown" class="dropdown-menu" >
                                        <li><a href="javascript:void(0)">BUY</a></li>
                                        <li><a href="javascript:void(0)">RENT</a></li>
                                        <li><a href="javascript:void(0)">GIVEAWAY</a></li>
                                        <li><a href="javascript:void(0)">SWAP</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:void(0)">ALL</a></li>          
                                    </ul>
                                </div>
                                <input type="text" class="form-control" value="" placeholder="Search..." name="s" id="s" style="border-color: #00B16A;">
                                <div class="input-group-btn">
                                    <button type="submit" id="searchsubmit" value="Search" class="btn" style="border-radius: 0;background-color: white;border-color: #00B16A;color:#00B16A">
                                        <span class="glyphicon glyphicon-search"></span></button>
                                </div>
                            </div>
                        </form> 

                    <!-- /.navbar-header -->


                    <!--<div class="collapse navbar-collapse upper-navbar">    	 
                            <ul id="menu-topmenu" class="nav navbar-nav navbar-right">
                                    <li id="menu-item-1"><a href="#one">Page I</a></li>
                                            <li id="menu-item-2"><a href="#two">Page II</a></li>
                            <li id="menu-item-3"><a href="#three">Page III</a></li>
                                    </ul>	  
                    </div><!-- /.navbar-collapse -->
                    <!--</div> -->
                </div>



                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class=" collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class = "row">
                        <ul class="nav navbar-nav navbar-right">

                            <li>

                                <a href="postlistings.html" style="color: white;padding-top:15px; "> 
                                    <strong> Post Listings</strong>
                                </a>

                            </li>

                            <li>
                                <a href="myListing.html">
                                    <!--<i class="fa fa-2x fa-bell wow bounceIn text-primary"></i> -->
                                    <img src="img/interface.png" alt="messages" style="height: 30px;">
                                </a>
                            </li>

                            <li>
                                <!--<a class="page-scroll" href="#portfolio"> -->
                                <!-- <div class="user" style="background-image:url('http://placehold.it/350x150')"></div> -->
                                <a href="#" >                                            <!--<i class="fa fa-4x fa-user wow bounceIn text-primary" data-wow-delay=".2s"></i> -->
                                    <img src="img/user_me.png" class="img-circle" alt="profile" style="height: 30px;">
                                </a>

                                <!-- /Users/vindhyakumarik/NetBeansProjects/TradeHub/web/img/IMG_5865.jpg -->

                            </li>
                            <li>
                                <!-- <a class="page-scroll" href="#services">  -->
                                <a href="#">
                                    <i class="fa fa-2x fa-caret-down bounceIn text-primary" ></i>
                                </a>
                            </li>
                            <li>
                                <a class="page-scroll"></a>
                            </li>

                            <!-- <li>
                                <a class="page-scroll" href="#contact">Contact</a>
                            </li> -->
                        </ul>
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
            </div>
        </nav>
        </div>
        <div class="container">
            <!-- main row -->
            <div class="row">

                <div class="col-md-4" style="background:#F1F3FA;">
                    <!-- Profile sidebar -->
                    <div class="row profile">
                        <div class="col-md-12">
                            <div class="profile-sidebar">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic">
                                    <img src="img/IMG_5865.jpg" class="img-responsive" alt="">
                                </div>
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name">
                                        Sandhya Kumari
                                    </div>
                                    <div class="profile-usertitle-job">
                                        Student
                                    </div>
                                </div>		
                            </div>
                        </div>
                    </div>

                    <div class="row profile">
                        <div class="col-md-12">
                            <div class="profile-sidebar">
                                <ul class="nav nav-pills nav-stacked">
                                    <li class="active"><a data-toggle="pill" href="#home"><i class="glyphicon glyphicon-home profilepadding"></i> Profile </a></li>
                          
                                    <li><a data-toggle="pill" href="#menu2"><i class="glyphicon glyphicon-ok profilepadding"></i> Listings <span class="badge">3</span></a></li>
                                    <li><a data-toggle="pill" href="#menu3"><i class="glyphicon glyphicon-ok profilepadding"></i> Communities </a></li>
                                </ul>			
                            </div>
                        </div>
                    </div>			
                </div>
                <div class="col-md-8">
                    <div class="tab-content menu-content">

                        <div id="home" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel">
                                        <div class="panel-heading" style="background:#FFFFFF; color: #00B16A;font-size: 15px;"><strong>Personal Details </strong><span class="pull-right"><i id="editpersonalinfo" class="fa fa-pencil profilespacing"></i> <button type="submit" id="savebuttonpers" class="btn btn-primary">Save</button></span>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="firstname">FirstName</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control" id="name" value="Sandhya" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="lastname">LastName</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control" id="lastname" value="Kumari" readonly>
                                                        </div>
                                                    </div>								  
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="occupation">Occupation</label>
                                                        <div class="col-sm-7"> 
                                                            <input type="text" class="form-control" id="occupation" value="Student" readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="panel">
                                        <div class="panel-heading" style="background:#FFFFFF; color: #00B16A;font-size: 15px;">
                                            <strong>Contact Information </strong><span class="pull-right"><i id="editcontactinfo" class="fa fa-pencil profilespacing"></i> <button type="submit" id="savebuttoncontact" class="btn btn-primary">Save</button></span>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="email">Email</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control" id="personalemailid" placeholder="sandhya@gmail.com" value="sandhya@gmail.com" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="address">Address</label>
                                                        <div class="col-sm-7">
                                                            <input type="text" class="form-control" id="personaladdress" placeholder="Enter address" value="Clemson, SC" readonly>
                                                        </div>
                                                    </div>								  
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-4" for="phone">Phone</label>
                                                        <div class="col-sm-7"> 
                                                            <input type="text" class="form-control" id="personalphone" placeholder="Enter phone" value="111-111-1111" readonly>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel">
                                        <div class="panel-heading" style="background:#FFFFFF; color: #00B16A;font-size: 15px;"><strong>Preferences </strong><span class="pull-right"><i id="editpersonalinfo" class="fa fa-pencil profilespacing"></i> <button type="submit" id="savebuttonpers" class="btn btn-primary">Save</button></span>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-7" for="firstname">Keep My Identity Private</label>
                                                        <div class="col-sm-3">
                                                            <input type="checkbox" class="form-control" id="checkprivacy" checked="true"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-7" for="lastname">Email Notifications</label>
                                                        <div class="col-sm-3">
                                                            <input type="checkbox" class="form-control" id="checkprivacy" checked="true" />
                                                        </div>
                                                    </div>								  
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-7" for="pwd">Current Status</label>
                                                        <div class="iconpadding col-sm-4"> 
                                                            <span class="label label-warning">Active</span> 
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="panel">
                                        <div class="panel-heading" style="background:#FFFFFF; color: #00B16A;font-size: 15px;"><strong>Communities</strong></div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <table class="table table-condensed">
                                                    <tbody>
                                                        <tr>
                                                            <td class="community-delete" id="northeastern"><img src="img/delete.png" class="img-responsive" alt=""></td>
                                                            <td>Clemson University</td>
                                                            <td>Boston, MA</td>
                                                        </tr>
                                                        <tr id="bostonrow">
                                                            <td class="community-delete" id="boston"><img src="img/delete.png" class="img-responsive" alt=""></td>
                                                            <td>Boston University</td>
                                                            <td>Boston, MA</td>
                                                        </tr>										  
                                                    </tbody>										
                                                </table>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <a href="#menu3"><h5>Add More +</h5></a>
                                                </div>
                                                <div class="col-md-2">
                                                </div>
                                                <div class="col-md-6">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>				
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <h3>Menu 1</h3>
                            <p></p>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <h3>Menu 2</h3>
                            <p></p>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <h3>Communities</h3>	


                            <div class="row communityspacing">
                                <div class="col-md-6">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="img/neu-seal.jpg" alt="CLEMSON"></img>
                                            </div>
                                            <div class="col-md-8">
                                                Founded in 1889, Clemson University consists of six colleges: Agriculture, Forestry and Life Sciences; Architecture, Arts and Humanities; Business and Behavioral Sciences; Engineering and Science; Health and Human Development; and Education. As of 2015, Clemson University enrolled a total of 18,016 undergraduate students for the fall semester and 4,682 graduate students and the student/faculty ratio is 16:1.
                                            </div>								
                                        </div>
                                        <div class="row communityspacing">
                                            <div class="col-md-2">

                                            </div>	
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-4">
                                                <button class="btn btn-success" type="button">
                                                    Joined <span class="glyphicon glyphicon-ok"></span>
                                                </button>
                                            </div>								
                                        </div>							
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="img/bu-seal.jpg" alt="BU"></img>
                                            </div>
                                            <div class="col-md-8">
                                                Boston University (most commonly referred to as BU or otherwise known as Boston U.) is a private research university located in Boston with 3,800 faculty members and 33,000 students.
                                            </div>								
                                        </div>
                                        <div class="row communityspacing">
                                            <div class="col-md-2">

                                            </div>	
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-4">
                                                <button class="btn btn-success" type="button">
                                                    Joined <span class="glyphicon glyphicon-ok"></span>
                                                </button>
                                            </div>								
                                        </div>								
                                    </div>						
                                </div>				
                            </div>

                            <div class="row communityspacing">
                                <div class="col-md-6">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="img/mit-seal.jpg" alt="CLEMSON"></img>
                                            </div>
                                            <div class="col-md-8">
                                                Community for Massachussets Institute Of Technology. The university in located in Cambridge, Massachussets.
                                            </div>								
                                        </div>
                                        <div class="row communityspacing">
                                            <div class="col-md-2">

                                            </div>	
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-4">
                                                <button class="btn btn-success" type="button">
                                                    Join <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </div>								
                                        </div>							
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img src="img/neu-seal.jpg" alt="NEU"></img>
                                            </div>
                                            <div class="col-md-8">
                                                Harvard University is devoted to excellence in teaching, learning, and research, and to developing leaders in many disciplines who make a difference globally. The University, which is based in Cambridge and Boston, Massachusetts, has an enrollment of over 20,000.
                                            </div>								
                                        </div>
                                        <div class="row communityspacing">
                                            <div class="col-md-2">

                                            </div>	
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-4">
                                                <button class="btn btn-success" type="button">
                                                    Join <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </div>								
                                        </div>							
                                    </div>						
                                </div>				
                            </div>				
                        </div>
                    </div>
                </div>
            </div>	

        </div>	

    </div>
</body>
</html>