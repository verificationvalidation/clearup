<%-- 
    Document   : productPage
    Created on : Apr 22, 2016, 7:36:15 PM
    Author     : sandhyakumarik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Clearup</title>

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css">

        <!-- Custom Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

        <!-- Plugin CSS -->
        <link rel="stylesheet" href="css/animate.min.css" type="text/css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/creative.css" type="text/css">
        <link rel="stylesheet" href="css/clearup.css" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js">

            < script src = "js/clearup.js" ></script>
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/jquery-1.10.2.js"></script>
<script>
            $(function () {
                $('#boston').on('click', function () {
                    $('#bostonrow').hide();

                });

                $('#sendmessagebtn').on('click', function () {
                    $('#beforesend').hide();
                    $('#aftersend').show();
                });

                $('#sendmessagbtn').on('click', function () {
                    $('#beforesend').show();
                    $('#aftersend').hide();
                });


            });
        </script>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body id="page-top">

        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
            <!--<div class="container-fluid"> -->
            <div style="background-color: #00B16A">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="row">
                <div class="col-lg-3 navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="homePage.html">Clearup</a>
                </div>


                <div class=" mid" role="navigation">
                    
                        <!--<a class="navbar-brand" href="http://localhost:8080/test/" title="My Project" rel="home">My Project</a> -->

                        <form class="navbar-form pull-left" role="search" method="get" id="searchform" action="http://localhost:8084/Clearup/searchResults.html">
                            <div class="input-group">
                                <div class="input-group-btn" >
                                    <button type="button" id="searchcategory" value="ALL" class="btn btn-primary dropdown-toggle" style="border-radius: 0;background-color: whitesmoke" data-toggle="dropdown" >ALL<span class="caret"></span></button>

                                    <ul id="searchdropdown" class="dropdown-menu" >
                                        <li><a href="javascript:void(0)">BUY</a></li>
                                        <li><a href="javascript:void(0)">RENT</a></li>
                                        <li><a href="javascript:void(0)">GIVEAWAY</a></li>
                                        <li><a href="javascript:void(0)">SWAP</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:void(0)">ALL</a></li>          
                                    </ul>
                                </div>
                                <input type="text" class="form-control" value="" placeholder="Search..." name="s" id="s" style="border-color: #00B16A;">
                                <div class="input-group-btn">
                                    <button type="submit" id="searchsubmit" value="Search" class="btn" style="border-radius: 0;background-color: white;border-color: #00B16A;color:#00B16A">
                                        <span class="glyphicon glyphicon-search"></span></button>
                                </div>
                            </div>
                        </form> 

                    <!-- /.navbar-header -->


                    <!--<div class="collapse navbar-collapse upper-navbar">    	 
                            <ul id="menu-topmenu" class="nav navbar-nav navbar-right">
                                    <li id="menu-item-1"><a href="#one">Page I</a></li>
                                            <li id="menu-item-2"><a href="#two">Page II</a></li>
                            <li id="menu-item-3"><a href="#three">Page III</a></li>
                                    </ul>	  
                    </div><!-- /.navbar-collapse -->
                    <!--</div> -->
                </div>



                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class=" collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class = "row">
                        <ul class="nav navbar-nav navbar-right">

                            <li>

                                <a href="postlistings.html" style="color: white;padding-top:15px; "> 
                                    <strong> Post Listings</strong>
                                </a>

                            </li>

                            <li>
                                <a href="Messages.jsp">
                                    <!--<i class="fa fa-2x fa-bell wow bounceIn text-primary"></i> -->
                                    <img src="img/interface.png" alt="messages" style="height: 30px;">
                                </a>
                            </li>

                            <li>
                                <!--<a class="page-scroll" href="#portfolio"> -->
                                <!-- <div class="user" style="background-image:url('http://placehold.it/350x150')"></div> -->
                                <a href="#" >                                            <!--<i class="fa fa-4x fa-user wow bounceIn text-primary" data-wow-delay=".2s"></i> -->
                                    <img src="img/user_me.png" class="img-circle" alt="profile" style="height: 30px;">
                                </a>

                                <!-- /Users/vindhyakumarik/NetBeansProjects/TradeHub/web/img/IMG_5865.jpg -->

                            </li>
                            <li>
                                <!-- <a class="page-scroll" href="#services">  -->
                                <a href="#">
                                    <i class="fa fa-2x fa-caret-down bounceIn text-primary" ></i>
                                </a>
                            </li>
                            <li>
                                <a class="page-scroll"></a>
                            </li>

                            <!-- <li>
                                <a class="page-scroll" href="#contact">Contact</a>
                            </li> -->
                        </ul>
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
            </div>
        </nav>



        <div class="container" style="padding-top: 30px">
            <div class="well well-sm" style="height:50px;">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <p class="text-left product-title">Cylo "Ultimate Urban Bicycle"</p>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <p class="text-right product-title">$1500</p>
                    </div>
                </div>
            </div>
        </div>


        <section class="no-padding no-gutter" id="portfolio">
            <div class="container-fluid">
                <div class="well-lg" style="border: 10px">
                    <div class="row" style="padding-left: 55px">

                        <div class="col-md-8">


                            <img src="img/5-Nike Bike.jpg" class="img-responsive" alt="" >

                            <div class="panel">


                                <!--   <div class="row fullwidth">
        <div class="columns small-12 slider">
          <div class="text-center slide" style="background: url('http://placehold.it/1600x650&text=[Slide1]');">
            <h1>Slide 1 heading</h1>
            <p>Slide 1 text</p>
          </div>
          <div class="text-center slide" style="background: url('http://placehold.it/1600x650&text=[Slide2]');">
            <h1>Slide 2 heading</h1>
            <p>Slide 2 text</p>
          </div>
        </div>
      </div> -->
                                <script type="text/javascript" src="js/jssor.slider.min.js"></script> 
                                <script>
            jssor_1_slider_init = function () {

                var jssor_1_options = {
                    $AutoPlay: true,
                    $AutoPlaySteps: 4,
                    $SlideDuration: 160,
                    $SlideWidth: 200,
                    $SlideSpacing: 3,
                    $Cols: 4,
                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$,
                        $Steps: 4
                    },
                    $BulletNavigatorOptions: {
                        $Class: $JssorBulletNavigator$,
                        $SpacingX: 1,
                        $SpacingY: 1
                    }
                };

                var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                //responsive code begin
                //you can remove responsive code if you don't want the slider scales while window resizing
                function ScaleSlider() {
                    var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                    if (refSize) {
                        refSize = Math.min(refSize, 809);
                        jssor_1_slider.$ScaleWidth(refSize);
                    }
                    else {
                        window.setTimeout(ScaleSlider, 30);
                    }
                }
                ScaleSlider();
                $Jssor$.$AddEvent(window, "load", ScaleSlider);
                $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
                //responsive code end
            };
                                </script>

                                <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 809px; height: 150px; overflow: hidden; visibility: hidden;">
                                    <!-- Loading Screen -->
                                    <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                                        <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                                        <div style="position:absolute;display:block;top:0px;left:0px;width:100%;height:100%;"></div>
                                    </div>
                                    <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 809px; height: 150px; overflow: hidden;">
                                        <div style="display: none;">
                                            <img data-u="image" src="img/5-Nike Bike.jpg" />
                                        </div>
                                        <div style="display: none;">
                                            <img data-u="image" src="img/5-1.jpg" />
                                        </div>
                                        <div style="display: none;">
                                            <img data-u="image" src="img/5-2.jpg" />
                                        </div>
                                        <div style="display: none;">
                                            <img data-u="image" src="img/5-3.jpg" />
                                        </div>
                                        <div style="display: none;">
                                            <img data-u="image" src="img/5-4.jpg" />
                                        </div>
                                        <div style="display: none;">
                                            <img data-u="image" src="img/5-5.jpg" />
                                        </div>
                                        <div style="display: none;">
                                            <img data-u="image" src="img/5-6.jpg" />
                                        </div>


                                        <a data-u="ad" href="http://www.jssor.com" style="display:none">jQuery Slider</a>

                                    </div>
                                    <!-- Bullet Navigator -->
                                    <div data-u="navigator" class="jssorb03" style="bottom:10px;right:10px;">
                                        <!-- bullet navigator item prototype -->
                                        <div data-u="prototype" style="width:21px;height:21px;">
                                            <div data-u="numbertemplate"></div>
                                        </div>
                                    </div>
                                    <!-- Arrow Navigator -->
                                    <span data-u="arrowleft" class="jssora03l" style="top:0px;left:8px;width:55px;height:55px;" data-autocenter="2"></span>
                                    <span data-u="arrowright" class="jssora03r" style="top:0px;right:8px;width:55px;height:55px;" data-autocenter="2"></span>
                                </div>
                                <script>
                                    jssor_1_slider_init();
                                </script>

                                <!-- #endregion Jssor Slider End -->



                            </div>




                        </div>
                        <div class="col-md-4">
                            <div class="text-left text-justify">
                                <p class="product-info">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <a href="checkOtherUserProfile.jsp" >

                                            <!--<i class="fa fa-4x fa-user wow bounceIn text-primary" data-wow-delay=".2s"></i> -->
                                            <img src="img/User.png" class="img-circle" style="height: 50px">
                                        </a>
                                    </div>
                                    <div class="col-lg-6" style="padding-top: 20px">
                                        Vashist Suresh
                                    </div>
                                </div>
                                </p>
                                <span class="stars">
                                    <span style="width: 55.48px;"></span>
                                </span>
                                <br>
                                <!--
                                <!--          $.fn.stars = function() {
                                  return $(this).each(function() {
                                      $(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 16));
                                  }); -->
                                <p class="product-desc" style="padding-right: 50px;font-style:bold">About this listing
                                </p>
                                <p class="product-desc" style="padding-right: 50px;">The CYLO One is dubbed the “Ultimate Urban Bicycle” and is designed to be the one bike for any and all city riding. To climb that steep hill of a goal, it’s made from a durable yet lightweight 6061 aluminum frame. A dynamo hub powers front and rear integrated lights, including a brake light, and Shimano mechanical disc brakes and a Gates Carbon Belt Drive with 3-speed Shimano Nexus hub keep it all tidy and efficient…
                                </p>
                               <!-- <div>
<label for="wishlist-button" class="btn btn-block btn-large wishlist-button-label wishlist-button-label-margin">
    <span class="rich-toggle-checked">
      <i class="icon icon-heart icon-rausch"></i>
      <span class="wishlist-button--control wishlist-button--detailed">
        Saved to Wish List
      </span>
    </span>
    <span class="rich-toggle-unchecked">
      <i class="icon icon-heart-alt icon-light-gray"></i>
      <span class="wishlist-button--control wishlist-button--detailed">
        Save to Wish List
      </span>
    </span>
</label> </div> -->
                               
                               <div class="panel book-it-panel" data-reactid=".ffbigjb4sg.1.0">
                                   <div class="panel-body panel-light" data-reactid=".ffbigjb4sg.1.0.0">
                                       <div class="row row-condensed space-3" data-reactid=".ffbigjb4sg.1.0.0.0">
                                           <div class="col-md-9" data-reactid=".ffbigjb4sg.1.0.0.0.0">
                                               <div class="row row-condensed" data-reactid=".ffbigjb4sg.1.0.0.0.0.0">
                                                   <div class="col-sm-6" data-reactid=".ffbigjb4sg.1.0.0.0.0.0.0">
                                                       <label class="book-it__label" data-reactid=".ffbigjb4sg.1.0.0.0.0.0.0.0">From Date</label>
                                                       <input type="text" name="checkin" class="checkin ui-datepicker-target" placeholder="mm/dd/yyyy" data-reactid=".ffbigjb4sg.1.0.0.0.0.0.0.1"></div>
                                                   <div class="col-sm-6" data-reactid=".ffbigjb4sg.1.0.0.0.0.0.1">
                                                       <label class="book-it__label" data-reactid=".ffbigjb4sg.1.0.0.0.0.0.1.0">To Date</label>
                                                       <input type="text" name="checkout" class="checkout ui-datepicker-target" placeholder="mm/dd/yyyy" data-reactid=".ffbigjb4sg.1.0.0.0.0.0.1.1"></div></div></div>
                                           <div class="col-md-3" data-reactid=".ffbigjb4sg.1.0.0.0.1">
                                               <div data-reactid=".ffbigjb4sg.1.0.0.0.1.0">
                                                  <!-- <label for="number_of_guests_12494334" class="book-it__label" data-reactid=".ffbigjb4sg.1.0.0.0.1.0.0">
                                                       <span data-reactid=".ffbigjb4sg.1.0.0.0.1.0.0.0">Guests</span>
                                                   </label>
                                                   <div class="select select-block" data-reactid=".ffbigjb4sg.1.0.0.0.1.0.1">
                                                       <select id="number_of_guests_12494334" name="number_of_guests" data-reactid=".ffbigjb4sg.1.0.0.0.1.0.1.0"><option selected="" value="1" data-reactid=".ffbigjb4sg.1.0.0.0.1.0.1.0.$1">1</option><option value="2" data-reactid=".ffbigjb4sg.1.0.0.0.1.0.1.0.$2">2</option>
                                                       </select></div> -->
                                               </div></div></div>
                                                   <div class="" data-reactid=".ffbigjb4sg.1.0.0.1">
                                                       <div class="book-it__subtotal" data-reactid=".ffbigjb4sg.1.0.0.1.0">
                                                           <table class="table" data-reactid=".ffbigjb4sg.1.0.0.1.0.0">
                                                               <tbody data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0">
                                                                   <tr class="price-item" data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$$100 x 7 days">
                                                                       <td data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$$100 x 7 nights.0">
                                                                           <span data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$$100 x 7 nights.0.0">$100 x 7 days</span>
                                                                           <span data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$$100 x 7 nights.0.1">&nbsp;</span></td>
                                                                       <td class="text-right price-item__price" data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$$100 x 7 nights.1">
                                                               <div class="" data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$$100 x 7 nights.1.0">
                                                                   <span data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$$100 x 7 nights.1.0.0">$700</span>
                                                               </div></td></tr>
                                                                   <!-- <tr class="price-item" data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$Service fee">
                                                                           <td data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$Service fee.0">
                                                                               <span data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$Service fee.0.0">Service fee</span>
                                                                               <span data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$Service fee.0.1">&nbsp;</span>
                                                                               <span data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$Service fee.0.2">
                                                                                   <i class="icon icon-question" data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$Service fee.0.2.0"></i>
                                                                                   <noscript data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$Service fee.0.2.1"></noscript></span>
                                                                           </td>
                                                                           
                                                                           
                                                                           <td class="text-right price-item__price" data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$Service fee.1">
                                                                               <div class="" data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$Service fee.1.0">
                                                                                   <span data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.0:$Service fee.1.0.0">$85</span></div></td></tr> -->
                                                                       <tr data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.1"><td data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.1.0">
                                                                               <span data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.1.0.0">Total</span></td>
                                                                           <td class="text-right" data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.1.1">
                                                                               <span data-reactid=".ffbigjb4sg.1.0.0.1.0.0.0.1.1.0">$700</span></td></tr></tbody></table></div>
                                                           <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#messagemodal" id="sendmessagbtn">Message Seller</button></div>
                                       <div class="book-it__message-container js-book-it-message dated_views_people" data-reactid=".ffbigjb4sg.1.0.0.2">
                                           <hr data-reactid=".ffbigjb4sg.1.0.0.2.0">
                                           <div class="listing-desirability" data-reactid=".ffbigjb4sg.1.0.0.2.1">
                                               <div class="icon-background-container icon-clock-background" data-reactid=".ffbigjb4sg.1.0.0.2.1.0">
                                                   <div class="book-it__message-text" data-reactid=".ffbigjb4sg.1.0.0.2.1.0.0">
                                                       <strong data-reactid=".ffbigjb4sg.1.0.0.2.1.0.0.0">
                                                           <span data-reactid=".ffbigjb4sg.1.0.0.2.1.0.0.0.0">People are eyeing this item.</span>
                                                       </strong><div class="media space-top-1" data-reactid=".ffbigjb4sg.1.0.0.2.1.0.0.1">
                                                           <span data-reactid=".ffbigjb4sg.1.0.0.2.1.0.0.1.0">24 others are looking at it for these dates.</span>
                                                       </div></div></div></div></div></div></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </section>
        
                        <div id="messagemodal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" id="resetbutton" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Message Seller</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-box">
                                            <div id="beforesend">
                                                <div class="form-bottom">
                                                    <form role="form" action="" method="post" class="registration-form" id="sendmessageform">
                                                        <div class="form-group">
                                                            <label for="form-user-name"><i>Seller Name</i></label>
                                                            <input type="text" name="form-user-name" placeholder="Sandhya" class="form-user-name form-control" id="form-user-name" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="sr-only" for="form-message-title">Message Title</label>
                                                            <input type="text" name="form-message-title" value="Cylo Ultimate Urban Bicycle" class="form-user-name form-control" id="form-message-title" required>
                                                        </div>
                                                        <!--<div class="form-group" id="community_desc" style="display:none">
                                                                <div class="well">
                                                                <h3>Northeastern</h3>
                                                                <h5>Community for the northeastern university</h5>
                                                                </div>
                                                        </div>-->

                                                        <div class="form-group" id="message">
                                                            <label class="sr-only" for="comment">Message Details</label>
                                                            <textarea class="form-control" rows="5" id="comment" placeholder="Message Details" required></textarea>
                                                        </div>							
                                                        <button type="button" id="sendmessagebtn" class="btn btn-success btn-sm">Send Message</button>
                                                    </form>
                                                </div>
                                            </div>
                                            <div id="aftersend" style="display:none;">
                                                <div class="form-bottom">
                                                    Thank you for contacting the seller.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

        <section class="no-padding no-gutter">
            <nav id="mainNav1" class="navbar navbar-bottom navbar-inverse">
                <!--<div class="container-fluid"> -->
                <div class="bg-dark">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-default">

                        <a class="navbar-brand page-scroll" href="#page-bottom">About Us</a>
                    </div>
                    <div class="navbar-default">

                        <a class="navbar-brand page-scroll" href="#page-bottom">Press</a>
                    </div>
                    <div class="navbar-default">

                        <a class="navbar-brand page-scroll" href="#page-bottom">Careers</a>
                    </div>
                    <div class="navbar-default">

                        <a class="navbar-brand page-scroll" href="#page-bottom">Blog</a>
                    </div>
                    <div class="navbar-default">

                        <a class="navbar-brand page-scroll" href="#page-bottom">Contact Us</a>
                    </div>
                </div>
            </nav>
        </section>

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/jquery.fittext.js"></script>
        <script src="js/wow.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/creative.js"></script>

    </body>

</html>

