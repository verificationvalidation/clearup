<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>ClearUp</title>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

        <!-- Custom Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

        <!-- Plugin CSS -->
        <link rel="stylesheet" href="css/animate.min.css" type="text/css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/creative.css" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            $(function () {
                var availableTags = [
                    "Clemson University",
                    "Paypal",
                    "Akamai",
                    "Boston University",
                    "Amazon"
                ];
                $("#tags").autocomplete({
                    source: availableTags,
                    appendTo: $('#communitylist')
                });

                $('#resetbutton').on('click', function () {
                    $('#signupmodal').find('input').val('');
                    $('#community_desc').hide();
                    $("#community_email").hide();
                });

                $("#passwordfield").on("keyup", function () {
                    if ($(this).val())
                        $(".glyphicon-eye-open").show();
                    else
                        $(".glyphicon-eye-open").hide();
                });
                $(".glyphicon-eye-open").mousedown(function () {
                    $("#form-password").attr('type', 'text');
                }).mouseup(function () {
                    $("#form-password").attr('type', 'password');
                }).mouseout(function () {
                    $("#form-password").attr('type', 'password');
                });
            });

            function populatedesc() {
                var str = $("#tags").val();
                if (str === "CLEMSON University") {
                    $("#emailext").html("@clemson.edu");
                } else if (str === "Boston University") {
                    $("#emailext").html("@boston.bu.edu");
                }
                $("#community_desc").show();
                $("#community_email").show();
            }
        </script>

    </head>

    <body id="page-top">

        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="#page-top">ClearUp</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a class="page-scroll" href="#about">About</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#services">Services</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#portfolio">Products</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contact">Contact</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

        <header>
            <div class="header-content">
                <div class="header-content-inner">
                    <hr/>
                    <h2>Rent, Buy, Swap and Giveaway the items</h2>
                    <a href="#about" class="btn btn-primary btn-xl page-scroll" data-toggle="modal" id="Sign Up" data-target="#signupmodal">Sign Up</a>
                    <a href="#about" class="btn btn-primary btn-xl page-scroll active" data-toggle="modal" data-target="#loginmodal">Login</a>
                </div>
            </div>
        </header>

        <div id="signupmodal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" id="resetbutton" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Register Now</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-box">
                            <div class="form-top">
                                <div class="form-top-left">
                                    <h3>Sign up now</h3>
                                    <p>Fill in the form below to get instant access:</p>
                                </div>
                            </div>
                            <div class="form-bottom">
                                <form role="form" action="" method="post" class="registration-form" id="communitylist">
                                    <div class="form-group">
                                        <label class="sr-only" for="form-user-name">User Name</label>
                                        <input type="text" name="form-user-name" placeholder="User name..." class="form-user-name form-control" id="form-user-name" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-last-name">Password</label>
                                        <input type="password" name="form-password-2" placeholder="Password.." class="form-password-2 form-control" id="form-password-2" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-email">Community</label>
                                        <input type="text" name="form-community" placeholder="Community..." class="form-email form-control" id="tags" onblur="populatedesc()" required>
                                    </div>
                                    <!--<div class="form-group" id="community_desc" style="display:none">
        <div class="well">
        <h3>Northeastern</h3>
                                            <h5>Community for the northeastern university</h5>
                                            </div>
    </div>-->

                                    <div class="form-group" id="community_email" >
                                        <label class="sr-only" for="form-email">Email ID</label>
                                        <input type="text" name="form-community" placeholder="Email" class="form-email form-control" id="emailid" required><span id="emailext"></span></input>
                                    </div>							
                                    <button type="submit" class="btn" id="signmeup">Sign me up!</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="loginmodal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Login to ClearUp</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-box">
                            <div class="form-top">
                                <div class="form-top-left">
                                    <p>Login to get instant access:</p>
                                </div>
                            </div>
                            <div class="form-bottom">
                                <form role="form" action="homePage.html" class="registration-form" id="communitylist">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-11"><label class="sr-only" for="form-user-name">UserName</label>
                                                <input type="text" name="form-user-name" placeholder="Username" class="form-user-name form-control" id="form-user-name" required></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">

                                            <div class="col-md-11"><input type="password" name="form-password" placeholder="Password" class="form-control" id="form-password" required></div>
                                            <div class="col-md-1"><span class="glyphicon glyphicon-eye-open" id="showpwd"></span></div>

                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Login</button>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <a href="#"><h5>I forgot my password</h5></a>
                                        </div>
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-6">
                                            <a href="#" data-toggle="modal" data-target="#signupmodal"><h5>Not a Member? Click here to Sign Up</h5></a>
                                        </div>									
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>	

        <section class="bg-primary" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 text-center">
                        <h2 class="section-heading">Install our application</h2>
                        <a href="#" class="btn btn-default btn-xl">Download Now</a>
                    </div>
                    <div class="col-lg-6 col-md-6 text-center">
                        <h2 class="section-heading">Install our application</h2>
                        <a href="#" class="btn btn-default btn-xl">Download Now</a>
                    </div>                
                </div>
            </div>
        </section>

        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Our Services</h2>
                        <hr class="primary">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-diamond wow bounceIn text-primary"></i>
                            <h3>Rent</h3>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-paper-plane wow bounceIn text-primary" data-wow-delay=".1s"></i>
                            <h3>Swap</h3>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-newspaper-o wow bounceIn text-primary" data-wow-delay=".2s"></i>
                            <h3>Sell</h3>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-heart wow bounceIn text-primary" data-wow-delay=".3s"></i>
                            <h3>Giveaway</h3>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="no-padding" id="portfolio">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <div class="col-lg-4 col-sm-6">
                        <a href="#" class="portfolio-box">
                            <img src="img/portfolio/1.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a href="#" class="portfolio-box">
                            <img src="img/portfolio/2.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a href="#" class="portfolio-box">
                            <img src="img/portfolio/3.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a href="#" class="portfolio-box">
                            <img src="img/portfolio/4.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a href="#" class="portfolio-box">
                            <img src="img/portfolio/5.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a href="#" class="portfolio-box">
                            <img src="img/portfolio/6.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <h2 class="section-heading">Let's Get In Touch!</h2>
                        <hr class="primary">
                        <p>Contact Us for any enquiries. Please post your suggestions also.</p>
                    </div>
                    <div class="col-lg-4 col-lg-offset-2 text-center">
                        <i class="fa fa-phone fa-3x wow bounceIn"></i>
                        <p>123-456-6789</p>
                    </div>
                    <div class="col-lg-4 text-center">
                        <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                        <p><a href="mailto:your-email@your-domain.com">feedback@clearup.com</a></p>
                    </div>
                </div>
            </div>
        </section>

        <!-- jQuery -->
        <!--<script src="js/jquery.js"></script>-->

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/jquery.fittext.js"></script>
        <script src="js/wow.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/creative.js"></script>


    </body>

</html>
